var express = require('express');
var router = express.Router();
const {clientTransaction} = require('./client')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('dealer', { title: 'Dealer Dash' });
});

router.get('/table', function(req, res, next) {
  res.render('table', { title: 'table' });
});

router.post('/dealwrite',function(req,res){ 
  const ordNum = req.body.OrdNumb;
  const make = req.body.CarMake;
  const model = req.body.CarModel;
  const color = req.body.Color;
  const dealerName = req.body.DealName;
  
  let client = new clientTransaction("admin.Dealer")
  client.init_connection("channel1","bmw-new")
  client.generateTransaction("raiseOrder",ordNum, make, model, color, dealerName).then((message)=>{
  console.log(message.toString())})  
});

router.post('/dealread',async function(req,res){
  const ordNum = req.body.OrderNumb;
  let client = new clientTransaction("admin.Dealer")
client.init_connection("channel1","-new")

 client.generateTransaction("readOrder",ordNum).then((message)=>{
     console.log(message.toString())
     res.send({ Cardata : message.toString() });

 })

  
});
router.post('/dealmatch',function(req,res){ 
  const ordNum = req.body.OrdNumb;
  const bmwVin = req.body.MVinNumb;
  //console.log(ordNum+vin+"is the data")
  let client = new clientTransaction("admin.Dealer")
  client.init_connection("channel1","bmw-new")
  client.generateTransaction("matchOrder",ordNum, bmwVin).then((message)=>{
  console.log(message)})  
});

module.exports = router;
