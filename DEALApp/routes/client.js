const yaml = require('js-yaml')
const fs = require('fs');
const {config} = require('./config')
const { FileSystemWallet, Gateway } = require('fabric-network');

const wallet = new FileSystemWallet(config["wallet"])
let connectionProfile = yaml.safeLoad(fs.readFileSync(config["connectionProfile"], 'utf8'));
let gateway = new Gateway()

class clientTransaction{
    constructor(userName){
        this.connectionOptions = {
            identity: userName,
            wallet: wallet,
            discovery: { enabled: true, asLocalhost: false }
        } 

    }

    init_connection(channelName,contractName){
        this.channel = channelName
        this.contractName = contractName
    }

    async generateTransaction(txnName,...args){
        try{
        await gateway.connect(connectionProfile,this.connectionOptions);
        let network = await gateway.getNetwork(this.channel);
        let contract = await network.getContract(this.contractName)
        let result = await contract.submitTransaction(txnName,...args);
        return result
        } catch (error) {

            console.log(`Error processing transaction. ${error}`);
            console.log(error.stack);
    
        } finally {
    
            // Disconnect from the gateway
            console.log('Disconnect from Fabric gateway.');
            gateway.disconnect();
    
        }
    }
}

module.exports = {
    clientTransaction
}
