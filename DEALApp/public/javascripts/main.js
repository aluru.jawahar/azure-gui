
function toTable() {
    window.location.href='/table';

}

function DealWriteData(){

    event.preventDefault();
    const ordNum = document.getElementById('OrdNum').value;
    const make = document.getElementById('carMake').value;
    const model = document.getElementById('model').value;
    const color = document.getElementById('color').value;
    const dealName = document.getElementById('dealerName').value;
    console.log(ordNum+make+model+color+dealName);

    if (ordNum.length==0||make.length==0||model.length==0||color.length==0||dealName.length==0) {
        alert("Please enter the data properly");
    }
    else{
        fetch('/dealwrite',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({OrdNumb: ordNum, CarMake: make, CarModel: model, Color: color, DealName: dealName})
        })
        .then(function(response){
            console.log(response);
            return response.json();
        })
        .then(function (data){
            console.log(JSON.stringify(data));
            alert(data.message);
        })
        .catch(function(err){
            console.log(err);
            alert("Error in processing request");
        })    
    }
}
function DealQueryData(){

    event.preventDefault();
    const ordNum = document.getElementById('QueryordNumbDeal').value;
    
    console.log(ordNum);

    if (ordNum.length==0) {
        alert("Please enter the data properly");
    }
    else{
        fetch('/dealread',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({OrderNumb: ordNum})
        })
        .then(function(response){
            console.log(response);
            return response.json();
        })
        .then(function (Cardata){
            dataBuf = Cardata["Cardata"]
            console.log(dataBuf)
            alert(dataBuf);
        })
        .catch(function(err){
            console.log(err);
            alert("Error in processing request");
        })    
    }
}
function matchOrderDeal(){

    event.preventDefault();
    const ordNum = document.getElementById('MOrdNum').value;
    const vin = document.getElementById('MvinNum').value;
    
    console.log(ordNum+vin);

    if (ordNum.length==0||vin.length==0) {
        alert("Please enter the data properly");
    }
    else{
        fetch('/dealmatch',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({OrdNumb: ordNum, MVinNumb: vin})
        })
        .then(function(response){
            console.log(response);
            return response.json();
        })
        .then(function (data){
            console.log(JSON.stringify(data));
            alert(data.message);
        })
        .catch(function(err){
            console.log(err);
            alert("Error in processing request");
        })    
    }
}