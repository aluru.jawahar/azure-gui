function toManuDash() {
    window.location.href='/manufacturer';

}


function ManWriteData(){

    event.preventDefault();
    const vin = document.getElementById('VinNumbMan').value;
    const make = document.getElementById('carMake').value;
    const model = document.getElementById('model').value;
    const color = document.getElementById('color').value;
    const dom = document.getElementById('dom').value;
    const flag = document.getElementById('flag').value;
    console.log(vin+make+model+color+dom+flag);

    if (vin.length==0||make.length==0||model.length==0||color.length==0||dom.length==0||flag.length==0) {
        alert("Please enter the data properly");
    }
    else{
        fetch('/manuwrite',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({VinNumb: vin, CarMake: make, CarModel: model, CarColor: color, DOM: dom, CarFlag: flag})
        })
        // .then(function(response){
        //     console.log(response);
        //     return response.json();
        // })

        .catch(function(err){
            console.log(err);
            alert("Error in processing request");
        })    
    }
}

function ManReadOrder(){

    event.preventDefault();
    const OrdNumb = document.getElementById('OrdNumber').value;

    console.log(OrdNumb);

    if (OrdNumb.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readOrder',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({OrdNumb})
        })
        .then(function(response){
            
            return response.json();
        })
        .then(function (Cardata){
            dataBuf = Cardata["Cardata"]
            console.log(dataBuf)
            alert(dataBuf);
        })
        .catch(function(err){
            console.log(err);
            alert("Error in processing request");
        })    
    }
}


function ManQueryData(){

    event.preventDefault();
    const Qvin = document.getElementById('QueryVinNumbMan').value;
    
    console.log(Qvin);

    if (Qvin.length==0) {
        alert("Please enter the data properly");
    }
    else{
        fetch('/manuread',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({QVinNumb: Qvin})
        })
        .then(function(response){
            console.log(response);
            return response.json();
        })
        .then(function (Cardata){
            dataBuf = Cardata["Cardata"]
            console.log(dataBuf)
            alert(dataBuf);
        })
        .catch(function(err){
            console.log(err);
            alert("Error in processing request");
        })    
    }
}


    function QData(val){

        event.preventDefault();
        
        console.log(val);
    
        
            fetch('/manuread',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',              
                },
                body: JSON.stringify({QVinNumb: val})
            })
            .then(function(response){
                console.log(response);
                return response.json();
            })
            .then(function (Cardata){
                dataBuf = Cardata["Cardata"]
                console.log(dataBuf)
                alert(dataBuf);
            })
            .catch(function(err){
                console.log(err);
                alert("Error in processing request");
            })    
        }

    
