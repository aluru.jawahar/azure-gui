let profile = {
    dealer: {
        "Wallet":"/home/kba/Desktop/kba-fabric-course/ClientApp/MANApp/routes/wallets/Dealer",
        "CCP": "/home/kba/Desktop/kba-fabric-course/ClientApp/MANApp/routes/wallets/connectionprofiles/Dealer.json"
    },
    mvd : {
        "Wallet":"/home/scar/Hyperledger-Fabric/kba-fabric-dev-course/ClientApp/MANApp/wallets/Mvd",
        "CCP": "<path to network folder>/gateways/Mvd/Mvd gateway.json"
    },
    manufacturer:{
        "Wallet":"/home/kba/Desktop/kba-fabric-course/ClientApp/MANApp/routes/wallets/Manufacturer/",
        "CCP": "/home/kba/Desktop/kba-fabric-course/ClientApp/MANApp/routes/wallets/connectionprofiles/Manufacturer.json"
    }


}

module.exports = {
    profile
}
