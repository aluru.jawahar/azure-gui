var express = require('express');
var router = express.Router();
const {clientApplication} = require('./client')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('manufacturer', { title: 'Manufacturer Dash' });
});

router.get('/table', function(req, res, next) {
  res.render('table', { title: 'table' });
});
router.get('/event', function(req, res, next) {
  res.render('event', { title: 'Event' });
});

router.post('/manuwrite',function(req,res){

  const vin = req.body.VinNumb;
  const make = req.body.CarMake;
  const model = req.body.CarModel;
  const color = req.body.CarColor;
  const DOM = req.body.DOM;
  const flag = req.body.CarFlag;
 console.log("Request is>>>>>>>>>>")
 
  let ManufacturerClient = new clientApplication();
 let ca = ManufacturerClient.setRoleAndIdentity("Manufacturer","admin.Manufacturer")
 console.log("CA>>>>>>>>>>",ca)
  ManufacturerClient.initChannelAndChaincode("channel1", "bmw-new");
  console.log("Transaction ready>>>>>");
  ManufacturerClient.generatedAndSubmitTxn(
      "addBmw",
      vin,make,model,color,DOM,flag
    )
    .then(message => {
      console.log("Response is ##########",message.toString());
    });
});

router.post('/manuread',async function(req,res){
  const Qvin = req.body.QVinNumb;
  let ManufacturerClient = new clientApplication();
  ManufacturerClient.setRoleAndIdentity("Manufacturer","admin.Manufacturer")
  ManufacturerClient.initChannelAndChaincode("channel1", "bmw-new");
  ManufacturerClient.generatedAndSubmitTxn("readBmw", Qvin).then(message => {
    console.log(message.toString());
    res.send({ Cardata : message.toString() });
  });

 })

 router.post('/readOrder',async function(req,res){
  const ordNum = req.body.OrdNumb;
  let ManufacturerClient = new clientApplication();
  ManufacturerClient.setRoleAndIdentity("Manufacturer","admin.Manufacturer")
  ManufacturerClient.initChannelAndChaincode("channel1", "bmw-new");
  ManufacturerClient.generatedAndSubmitTxn("readOrder", ordNum).then(message => {
    console.log("Order is",message.toString())
    res.send({ Cardata : message.toString()});
  });

 })

//  router.post('/readOrder',function(req,res){ 
//   const ordNum = req.body.OrdNumb;
//   let ManufacturerClient = new clientApplication();
//   //console.log(ordNum+vin+"is the data")
//   ManufacturerClient.setRoleAndIdentity("Manufacturer","admin.Manufacturer")
//   ManufacturerClient.initChannelAndChaincode("channel1", "bmw");
//   ManufacturerClient.generateTransaction("readOrder",ordNum).then((message)=>{
//   console.log("Order is",message.toString())
//   res.send(message.toString());
// })  
// });



module.exports = router;


